#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class MyOwnClass 
{
public:
	MyOwnClass() 
	{ 
		ClassField = "����������� ��� ���������"; 
	}
	MyOwnClass(string _substring) : ClassField(_substring)
	{}
	void PrintBuilder() 
	{
		cout <<endl<< ClassField;
	}

private:
	string ClassField;
};
class Vector 
{
private:
	double x, y, z;
public:
	Vector() : x(0), y(0), z(0) {}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z) { }
	void PrintVectors() 
	{
		cout << endl << x << "   " << y << "   " << z << "   ";
	}
	double VectorLength() 
	{
		return sqrt(pow(x,2)+ pow(y, 2)+ pow(z, 2));
	}
};

void main()
{
	setlocale(LC_ALL, "Russian");
	MyOwnClass object1(" ������� ��� � ����� �������� \n � ������ ���� � � ����� ��������! \n �������� � ������ ������������� \n � � ���� ���������� ��������!\n");
	object1.PrintBuilder();

	Vector objVector(12, 13, 25);
	cout <<endl<< "������ � ������������: ";
	objVector.PrintVectors();
	cout << endl << objVector.VectorLength()<<" - ������ �������";
}